import React, { Children } from "react";
import { render, screen } from "@testing-library/react";
import Chat from "./Chat";

const URL: string =
  "https://edikdolynskyi.github.io/react_sources/messages.json";

test("renders learn react link", () => {
  render(<Chat url={URL} />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
