import React, { useEffect, useState } from "react";
import { Header } from "./Header/Header";
import { Preloader } from "./Preloader/Preloader";
import { MessageList } from "./MessageList/MessageList";
import { MessageInput } from "./MessageInput/MessageInput";
import {
  countUsers,
  getLastDate,
  getSortedMessages,
  loadAll,
} from "../helpers/helpers";
import { IMessage } from "../common/interfaces/interfaces";
import * as USER from "../common/constants/constants";

import "./Chat.scss";

interface Props {
  url: string;
}

const Chat: React.FC<Props> = ({ url }) => {
  const [messages, setMessages] = useState<Array<IMessage>>([]);
  const [isLoading, setLoading] = useState<boolean>(true);
  const [editMessage, setEditMessage] = useState<IMessage | null>(null);

  const getAllMessage = () => {
    setLoading(true);
    loadAll(url, setMessages);
    setLoading(false);
  };

  useEffect(getAllMessage, [url]);

  const handleAddMessage = (message: IMessage): void => {
    setMessages([...messages, message] as IMessage[]);
  };

  const handleDeleteMessage = (id: string): void => {
    const updatedMessages: IMessage[] = messages.filter(
      (message) => message.id !== id
    );
    setMessages(updatedMessages);
  };

  const handleEditMessage = (message: IMessage): void => {
    const newMessages: IMessage[] = messages.map((oldMessage) => {
      if (oldMessage.id === message.id) {
        return message;
      }
      return oldMessage;
    });
    setMessages(newMessages);
    setEditMessage(null);
  };

  const getEditMessage = (id: string): void => {
    const updateMessage: IMessage | undefined = messages.find(
      (message) => message.id === id
    );
    if (updateMessage) {
      setEditMessage(updateMessage);
    }
  };

  return isLoading ? (
    <Preloader></Preloader>
  ) : (
    <div className="chat">
      <Header
        usersCount={countUsers(messages)}
        messagesCount={messages.length}
        lastMessageDate={getLastDate(messages)}
      ></Header>
      <MessageList
        messages={getSortedMessages(messages)}
        user={USER.NAME}
        handleDeleteMessage={handleDeleteMessage}
        getEditMessage={getEditMessage}
      ></MessageList>
      <MessageInput
        message={editMessage}
        user={USER.NAME}
        userId={USER.ID}
        handleAddMessage={handleAddMessage}
        handleEditMessage={handleEditMessage}
      ></MessageInput>
    </div>
  );
};

export default Chat;
