import "./Header.scss";

interface Props {
  usersCount: number;
  messagesCount: number;
  lastMessageDate: string;
}

export const Header: React.FC<Props> = ({
  usersCount,
  messagesCount,
  lastMessageDate,
}) => {
  return (
    <header className="header">
      <h1 className="header-title">Web Chat</h1>
      <div className="header-users-count">{usersCount} participants</div>
      <div className="header-messages-count">{messagesCount} messeges</div>
      <div className="header-last-message-date">{lastMessageDate}</div>
    </header>
  );
};
