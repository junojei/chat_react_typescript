import { useState } from "react";
import { IMessage } from "../../common/interfaces/interfaces";
import { getTime } from "../../helpers/helpers";
import "./Message.scss";

interface Props {
  message: IMessage;
}

export const Message: React.FC<Props> = ({ message }) => {
  const { text, createdAt, editedAt, user, avatar }: IMessage = message;
  const [isLike, setLike] = useState<boolean>(false);

  const onLike = () => setLike(!isLike);

  return (
    <div className="message">
      <div className="message-user-avatar-wrapper">
        <img className="message-user-avatar" src={avatar} alt="avatar" />
      </div>
      <div className="message-info">
        <div className="message-user-name">{user}</div>
        <div className="message-text">{text}</div>
        <div className="message-time">
          {editedAt ? "edit " + getTime(editedAt) : getTime(createdAt)}
        </div>
      </div>
      <div className="message-like" onClick={onLike}>
        <i className={isLike ? "fas fa-heart" : "far fa-heart"}></i>
      </div>
    </div>
  );
};
