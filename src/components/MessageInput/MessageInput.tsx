import React, { useEffect, useState } from "react";
import { IMessage } from "../../common/interfaces/interfaces";
import { getFullDate } from "../../helpers/helpers";
import "./MessageInput.scss";

interface Props {
  message?: IMessage | null;
  user: string;
  userId: string;
  handleAddMessage: (message: IMessage) => void;
  handleEditMessage: (message: IMessage) => void;
}

export const MessageInput: React.FC<Props> = ({
  message,
  user,
  userId,
  handleAddMessage,
  handleEditMessage,
}) => {
  const [text, setText] = useState("");

  useEffect(() => {
    message ? setText(message.text) : setText("");
  }, [message]);

  const addMessage = (e: React.FormEvent) => {
    e.preventDefault();
    if (text) {
      const newMessage = {
        id: Date.now() + "",
        userId: userId,
        avatar:
          "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
        user: user,
        text: text,
        createdAt: getFullDate(new Date()),
      } as IMessage;
      handleAddMessage(newMessage);
      setText("");
    }
  };

  const editedMessage = (e: React.FormEvent) => {
    e.preventDefault();
    if (text) {
      const newMessage = {
        ...message,
        text,
        editedAt: getFullDate(new Date()),
      } as IMessage;
      handleEditMessage(newMessage);
      setText("");
    }
  };

  return (
    <form
      className="message-input"
      onSubmit={message ? editedMessage : addMessage}
    >
      <textarea
        className="message-input-text"
        placeholder="Message"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <button className="message-input-button" type="submit">
        {message ? "Edit" : "Send"}
      </button>
    </form>
  );
};
