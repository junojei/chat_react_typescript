import { getDateText } from "../../../helpers/helpers";
import "./Divider.scss";

interface Props {
  date: string;
}

export const Divider: React.FC<Props> = ({ date }) => {
  return (
    <div className="messages-divider">
      <div className="messages-divider-date">{getDateText(date)}</div>
    </div>
  );
};
