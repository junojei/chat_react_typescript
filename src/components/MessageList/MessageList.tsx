import React, { useState, useRef, useEffect } from "react";
import { IMessage } from "../../common/interfaces/interfaces";
import { getGroupedMessages } from "../../helpers/helpers";
import { Message } from "../Message/Message";
import { OwnMessage } from "../OwnMessage/OwnMessage";
import { Divider } from "./Divider/Divider";
import "./MessageList.scss";

interface Props {
  messages: IMessage[];
  user: string;
  handleDeleteMessage: (id: string) => void;
  getEditMessage: (id: string) => void;
}

export const MessageList: React.FC<Props> = ({
  messages,
  user,
  handleDeleteMessage,
  getEditMessage,
}) => {
  const [groupedMessages, setGroupedMessages] = useState<
    Array<[string, Array<IMessage>]>
  >([]);
  const listRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (listRef.current) {
      listRef.current.scrollTop = listRef.current.scrollHeight;
    }
  }, [groupedMessages]);

  useEffect(() => {
    setGroupedMessages(getGroupedMessages(messages));
  }, [messages]);

  return (
    <main ref={listRef} className="message-list">
      {groupedMessages.map((days) => {
        return (
          <div className="message-list-day-wrapper" key={days[0]}>
            <Divider date={days[0]} />
            {days[1].map((message) => {
              if (message.user === user) {
                return (
                  <OwnMessage
                    key={message.id}
                    message={message}
                    handleDeleteMessage={handleDeleteMessage}
                    getEditMessage={getEditMessage}
                  />
                );
              }
              return <Message key={message.id} message={message} />;
            })}
          </div>
        );
      })}
    </main>
  );
};
