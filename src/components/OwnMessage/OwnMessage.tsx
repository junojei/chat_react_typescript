import { IMessage } from "../../common/interfaces/interfaces";
import { getTime } from "../../helpers/helpers";
import "./OwnMessage.scss";

interface Props {
  message: IMessage;
  handleDeleteMessage: (id: string) => void;
  getEditMessage: (id: string) => void;
}

export const OwnMessage: React.FC<Props> = ({
  message,
  handleDeleteMessage,
  getEditMessage,
}) => {
  const { text, createdAt, editedAt, id }: IMessage = message;

  const onDeleteMessage = () => handleDeleteMessage(id);
  const onEditMessage = () => getEditMessage(id);

  return (
    <div className="own-message">
      <div className="message-text">{text}</div>
      <div className="message-time">
        {editedAt ? "edit " + getTime(editedAt) : getTime(createdAt)}
      </div>
      <div className="message-button-wrapper">
        <button className="message-edit" onClick={onEditMessage}>
          Edit
        </button>
        <button className="message-delete" onClick={onDeleteMessage}>
          Delete
        </button>
      </div>
    </div>
  );
};
