import "./Preloader.scss";

export const Preloader: React.FC = () => {
  return (
    <main>
      <div className="preloader"></div>
    </main>
  );
};
