import { IMessage } from "../../common/interfaces/interfaces";

export const getFullDate = (date: string | Date): string => {
  const fullDate = typeof date === "string" ? new Date(date) : date;

  const day = fullDate.getDay();
  const mounth = fullDate.getMonth();
  const year = fullDate.getFullYear();
  const hours = fullDate.getHours();
  const minutes = fullDate.getMinutes();
  const seconds = fullDate.getSeconds();

  return `${day < 10 ? "0" + day : day}.${
    mounth < 10 ? "0" + mounth : mounth
  }.${year} ${hours < 10 ? "0" + hours : hours}:${
    minutes < 10 ? "0" + minutes : minutes
  }:${seconds < 10 ? "0" + seconds : seconds}`;
};

export const getDateText = (date: string): string => {
  const dateNow = new Date().getDate();
  const inputDate = +date.replace(/\D/g, "");

  switch (dateNow - inputDate) {
    case 0:
      return "Today";
    case 1:
      return "Yesterday";
    default:
      return date;
  }
};

export const getTime = (date: string): string => {
  const fullDate = new Date(date);

  const hours = fullDate.getHours();
  const minutes = fullDate.getMinutes();

  return `${hours < 10 ? "0" + hours : hours}:${
    minutes < 10 ? "0" + minutes : minutes
  }`;
};

export const getLastDate = (messages: IMessage[]): string => {
  if (messages.length === 0) {
    return "No messages";
  }

  const dates = messages.map((el) => el.createdAt);
  let result: string = "";

  dates.forEach((el) => {
    result = getDiff(result, el);
  });

  return getFullDate(result);
};

export const getDiff = (dateA: string, dateB: string): string => {
  const diff = Date.parse(dateA) - Date.parse(dateB);
  return diff >= 0 ? dateA : dateB;
};

export const getDiffFromToday = (date: string): number => {
  const diff = new Date().getDay() - new Date(date).getDay();
  return diff;
};
