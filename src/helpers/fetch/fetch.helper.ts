import { IMessage } from "../../common/interfaces/interfaces";

export const loadAll = async (url: string, setFunc: Function) => {
  return fetch(url)
    .then((res) => res.json())
    .then((data) => {
      setFunc(data as IMessage[]);
    })
    .catch((err) => {
      console.log("Download error -> ", err);
      setFunc([]);
    });
};
