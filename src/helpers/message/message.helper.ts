import { IMessage } from "../../common/interfaces/interfaces";

export const getSortedMessages = (messages: IMessage[]) => {
  return messages.sort(
    (msgA, msgB) => Date.parse(msgA.createdAt) - Date.parse(msgB.createdAt)
  );
};

export const getGroupedMessages = (messages: IMessage[]) => {
  const map = new Map();

  messages.forEach((message) => {
    const messsageDate = new Date(message.createdAt).getDate();
    const messsageMonth = new Date(message.createdAt).toLocaleString("en", {
      month: "long",
    });
    const messageDay = new Date(message.createdAt).toLocaleString("en", {
      weekday: "long",
    });
    const date = `${messageDay}, ${messsageDate} ${messsageMonth}`;

    if (map.has(date)) {
      const day = map.get(date);
      map.set(date, [...day, message]);
    } else {
      map.set(date, [message]);
    }
  });
  return Array.from(map.entries());
};
