import { IMessage } from "../../common/interfaces/interfaces";

export const countUsers = (messages: IMessage[]): number => {
  const users = messages.map((el) => el.user);
  const result: string[] = [];

  users.forEach((el) => {
    if (!result.includes(el)) {
      result.push(el);
    }
  });

  return result.length;
};
